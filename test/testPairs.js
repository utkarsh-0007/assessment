import pairs from '../Problems/pairs.js'; // Importing the pairs function from its module
import testObject from "../object.js";

const result = pairs(testObject);
if(result.length === 0)
console.log("The Object has no properties");
else
console.log(`The Key and Value pairs ${result}`);
