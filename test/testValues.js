import values from "../Problems/values.js";
import testObject from "../object.js";

const result = values(testObject);

if(result.length === 0){
    console.log('The Object has no properties');
}
else{
    console.log(`The values of the Object are ${result}`);
}