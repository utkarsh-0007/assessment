import keys from "../Problems/keys.js";
import testObject from "../object.js";

const result = keys(testObject);

if (result.length === 0) {
    console.log("The Object has no properties");
} else {
    console.log(`The Object has ${result} as properties`);
}
