import invert from '../Problems/invert.js'; 
import testObject from '../object.js';

const result = invert(testObject);

if(result === null)
console.log("The Object is null");
else
console.log(result);
