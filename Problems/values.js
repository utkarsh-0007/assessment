import keys from "./keys.js";
export default function values(obj) {
    if(obj == null)
    return [];
    let _keys = keys(obj); 
    return _keys.filter(key => typeof obj[key] !== 'function').map(key => obj[key]); 
  }